# Catalog Service #

A sample node.js microservice leveraging [Seneca](http://senecajs.org/) and [Elastic Search](https://www.elastic.co/products/elasticsearch)

Part of a larger demo: https://bitbucket.org/coxautomotive/books-api