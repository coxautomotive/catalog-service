module.exports.load = function(client, cb) {
  var retryCount = 0;
  var maxRetries = 20;

  var interval = setInterval(function() {
    client.ping({
      hello: "elasticsearch!"
    }, function (error) {
      if (error) {
        if (++retryCount === maxRetries) {
          cb(error);
        }
      } else {
        clearInterval(interval);
        client.update({
          index: 'catalog',
          type: 'book',
          id: '1234',
          body: {
            doc: {
              title: 'Building Microservices',
              author: 'Sam Newman',
              isbn: '1491950358',
              pageCount: 280,
              published: new Date('February 20, 2015')
            },
            doc_as_upsert : true
          }
        }, cb);
      }
    });
  }, 1000);
};
