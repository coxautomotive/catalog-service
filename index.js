var seneca = require('seneca')();
var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
  host: 'elasticsearch:9200'
});

seneca.add({role: 'catalog', cmd: 'getById'}, function(msg, respond) {

  client.get({
    index: 'catalog',
    type: 'book',
    id: 1234
  }, function (error, response) {
    respond(error, response._source);
  });

});

var samples = require('./samples.js');
samples.load(client, function(err) {
  if (err) {
    console.log('ERROR: catalog-service - ' + err);
  } else {
    seneca.listen();
    seneca.ready(function() {
      console.log('catalog-service ready!');
    });
  }
});
